#!env/bin/python
import sys

sys.path.append('.')
from argparse import ArgumentParser
from surveystore.api.dataminer import AllScriptDataMiner,\
    FakeSurveryAnswerGenerator, MongoSurveyReader
from surveystore.api.http import WebService
from pymongo import MongoClient

from surveystore.logger import Logger

import json

logger = Logger.get_logger(__name__)


def _do_sth():
    parser = ArgumentParser()
    parser.add_argument('-f', '--file', dest='filename', default='/tmp/walk.json')
    parser.add_argument('-d', '--dataset', dest='dataset', default='walk')
    parser.add_argument('-c', '--cleanupdb', dest='cleanupdb', action="store_true")
    parser.add_argument('-a', '--action', dest='action', default="populatedb")

    args = parser.parse_args()
    config = json.loads(open('resources/ehr.json').read())
    url = config['server']
    svc_username = config['serviceusername']
    svc_password = config['servicepassword']
    app_name = config['appname']

    api = WebService(base_url=url,
                     username=svc_username,
                     password=svc_password,
                     app_name=app_name,
                     cache_token=True)
    db = MongoClient(config['mongohost'],
                     int(config['mongoport'])).surveystore

    with open('resources/survey.json') as f:
        questions = json.loads(f.read())

    if args.cleanupdb:
        db.messages.remove({})
        db.surveys_2.remove({})
        db.schedules.remove({})

    if args.action == "populatedb":

        asdm = AllScriptDataMiner(ehr_api=api, mongo_db=db,
                                  ehr_username=config['ehr_username'], questions=questions)
        asdm.check_schedule_and_create_messages('9/1/2015', '10/4/2015')
        fake = FakeSurveryAnswerGenerator(db)
        fake.respond_to_all_surverys()
        fake.responsd_to_claims_surverys()
        fake.responsd_to_appointment_survery()
        fake.respond_to_new_question()
        reader = MongoSurveyReader(db)
        reader.read()
        surveys = db.surveys.find_one({})
        logger.debug('surveys has %s records' % surveys)

    if args.action == "fakenewresponse":
        fake = FakeSurveryAnswerGenerator(db)
        fake.respond_to_new_question()

    if args.action == "sendmessage":
        asdm = AllScriptDataMiner(ehr_api=api, mongo_db=db,
                                  ehr_username=config['ehr_username'], questions=questions)
        asdm.send_message(config['twilio_account'], config['twilio_auth'])

if __name__ == "__main__":
    _do_sth()
