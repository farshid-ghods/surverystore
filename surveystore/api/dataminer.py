from bson.objectid import ObjectId
from surveystore.logger import Logger
import random
import requests
import pprint
import uuid
from faker import Factory

logger = Logger.get_logger(__name__)


# data minor : get schedule and for new schedules then remeber the patient id and
# wen to send text message
# alos for the appointment itself send a text message
# on the day of the appointment then find out the medication for that patient


class SurveryType(object):
    SCHEDULE = "SCHEDULE"
    VISIT = "VISIT"
    PRESCRIPTION = "PRESCRIPTION"


class SurveyMessageType(object):
    CREATED = "CREATED"
    SENT = "SENT"
    RECEIVED = "RECEIVED"


class FakeSurveryAnswerGenerator(object):
    def __init__(self, mongo_db):
        self._m_messages = mongo_db.messages
        self._m_schedules = mongo_db.schedules
        self._m_surveys = mongo_db.surveys_2

    def _fake_date(self):
        faker = Factory.create()
        year = 2015
        month = "%02d" % 10
        day = "%02d" % random.randint(1, 15)
        return '%s/%s/%s %s PM' % (month, day, year, faker.time())

    def responsd_to_claims_surverys(self, count=200):
        counter = 0
        faker = Factory.create()
        for i in range(0, count):
            doc = {
                "PatientID": random.randint(1, 2000),
                "Location": "New World Health - Main Location",
                "AppointmentTime": self._fake_date(),
                "AppointmentDate": faker.date(),
                "EncounterID": random.randint(1000, 4000),
                "Question": "HOW WAS YOUR EXPERIENCE AS AN OUTPATIENT VISIT",
                "Answer": random.randint(1, 10),
                "Category": "OUTPATIENT",
                "UUID": str(uuid.uuid4())[0:4]
            }
            self._m_surveys.insert(doc)
            counter = counter + 1
        logger.info("responded to %s records" % counter)

    def responsd_to_appointment_survery(self, count=200):
        counter = 0
        faker = Factory.create()
        for i in range(0, count):
            doc = {
                "PatientID": random.randint(1, 2000),
                "Location": "New World Health - Main Location",
                "AppointmentTime": self._fake_date(),
                "AppointmentDate": faker.date(),
                "EncounterID": random.randint(1000, 4000),
                "Question": "HOW WAS YOUR EXPERIENCE IN SCHEDULINGS AN APPOINTMENT",
                "Answer": random.randint(1, 10),
                "Category": "OUTPATIENT",
                "UUID": str(uuid.uuid4())[0:4]
            }
            self._m_surveys.insert(doc)
            counter += 1
        logger.info("responded to %s records" % counter)

    def respond_to_new_question(self, count=3):
        counter = 0
        faker = Factory.create()
        for i in range(0, count):
            doc = {
                "PatientID": random.randint(1, 2000),
                "Location": faker.address(),
                "AppointmentTime": self._fake_date(),
                "AppointmentDate": faker.date(),
                "EncounterID": random.randint(1000, 4000),
                "Question": "HOW WAS YOUR EXPERIENCE IN THE FOLLOW UP VISITS",
                "Answer": random.randint(1, 10),
                "Category": "OUTPATIENT",
                "UUID": str(uuid.uuid4())[0:4]
            }
            self._m_surveys.insert(doc)
            counter += 1
        logger.info("responded to %s records" % counter)

    def respond_to_all_surverys(self):
        '''
        go throuhg all the messages and fill the survery answer data
        :return:
        '''
        # created_messages = self._m_messages.find({"MessageStatus": "CREATED"})
        faker = Factory.create()
        created_messages = self._m_messages.find({})
        counter = 0
        for msg in created_messages:
            encounter_id = msg["EncounterID"]
            schedule = self._m_schedules.find_one({"EncounterID": encounter_id})
            answer = random.randint(1, 10)
            if schedule:
                try:
                    doc = {
                        "PatientID": str(schedule["PatientID"]),
                        "Location": faker.address(),
                        "AppointmentTime": self._fake_date(),
                        "AppointmentDate": faker.date(),
                        "EncounterID": str(schedule["EncounterID"]),
                        "Question": str(msg["MessageRawQuestion"]),
                        "Answer": answer,
                        "Category": str(msg["SurveyType"]),
                        "UUID": str(uuid.uuid4())[0:4]
                    }
                    logger.info(doc)
                    self._m_surveys.insert(doc)
                    counter += 1
                    msg["MessageAnswer"] = answer
                    msg["MessageStatus"] = "RECEIVED"
                    self._m_messages.update({'_id': ObjectId(msg['_id'])}, msg)
                except Exception as ex:
                    logger.exception(ex)
        logger.info("responded to %s records" % counter)


class MongoSurveyReader(object):
    def __init__(self, mongo_db):
        self._m_messages = mongo_db.messages
        self._m_surveys = mongo_db.surveys_2

    def read(self):
        created_messages = self._m_surveys.find({"MessageStatus": "RECEIVED"})
        for msg in created_messages:
            logger.info(msg)


class AllScriptDataMiner(object):
    def __init__(self, ehr_api, mongo_db, ehr_username, questions):
        self._mongo_db = mongo_db
        self._ehr_api = ehr_api
        self._ehr_username = ehr_username
        self._m_schedules = mongo_db.schedules
        self._m_messages = mongo_db.messages
        self._questions = questions

    def _get_phone_digits(self, phone_number):
        number = ''
        for d in phone_number:
            if d.isdigit():
                number = number + d
        return number

    def _phone_number(self, patient_id):
        patient = self._ehr_api.get_patient(self._ehr_username,
                                            patient_id)
        if patient:
            if 'CellPhone' in patient[0] and patient[0]['CellPhone']:
                return self._get_phone_digits(patient[0]['CellPhone'])
            if 'PhoneNumber' in patient[0] and patient[0]['PhoneNumber']:
                return self._get_phone_digits(patient[0]['PhoneNumber'])
        raise Exception('unable to find phone number for patient %s' % patient)

    def _get_question(self, category, rank=1):
        if not category:
            raise ValueError('category can not be null')
        if not category in self._questions:
            raise Exception('input json does not have questions for category %s' % category)
        question = self._questions[category]
        for item in question['QUESTIONS']:
            if item['RANK'] == rank:
                return item['TITLE']
        raise Exception('input json does not have questions for category %s' % category)

    def send_message(self, twilio_account, twilio_auth):
        created_messages = self._m_messages.find({"MessageStatus": "RECEIVED"})
        phone_numbers = ['3158773098']
        counter = 0
        url = 'https://api.twilio.com/2010-04-01/' + \
              'Accounts/%s/Messages.json' % twilio_account
        for msg in created_messages:
            body = "From: New Health Facility(%s).How Was Your Experience In The Doctor's Follow Up Visit On Scale of 1-10" % \
                   msg[
                       "EncounterID"]
            params = {'To': phone_numbers[counter],
                      'From': '13153148389',
                      'Body': body}
            response = requests.post(url=url, auth=(twilio_account, twilio_auth), data=params)
            logger.debug(response.text)
            counter += 1
            if counter == 3:
                break

    def create_message_record(self, allehr_schedule):
        phone_number = self._phone_number(allehr_schedule['patientID'])

        m_doc = {"PatientID": allehr_schedule['patientID'],
                 "EncounterID": allehr_schedule['Encounterid'],
                 "MessagePhoneNumber": phone_number,
                 "MessageRawQuestion": self._get_question(SurveryType.VISIT),
                 "MessageQuestion": self._get_question(SurveryType.VISIT),
                 "MessageRawAnswer": "",
                 "MessageAnswer": "",
                 "MessageStatus": "CREATED",
                 "MessageReceivedDate": "",
                 "SurveyType": SurveryType.VISIT
                 }
        self._m_messages.insert_one(m_doc)

    def check_schedule_and_create_messages(self, start_date, end_date):
        '''
            invoke getschedule and for each schedule
            check if we have an entry of that in mongo
            and if we do has the appointment time changed ?
            if it has changed then update the appointment date
            what do we store in mongo: encounter_id, patient_id, appointment_time
        :return:
        '''
        date_filter = start_date
        if end_date:
            date_filter = date_filter + '|' + end_date
        schedules = self._ehr_api.get_schedule(ehr_username=self._ehr_username,
                                               start_date=date_filter,
                                               changed_since='',
                                               include_pix='',
                                               other_user='',
                                               appointment_types='',
                                               status_filter='')
        for schedule in schedules:
            # logger.debug(pprint.pformat(schedule))
            stored = self._m_schedules.find_one({'Encounterid': schedule['Encounterid']})
            if stored:
                if not stored['AppointmentTime'] == schedule['ApptTime']:
                    doc = {'EncounterID': schedule['Encounterid'],
                           'PatientID': schedule['patientID'],
                           "Location": schedule["Location"],
                           'AppointmentTime': schedule['ApptTime']
                           }
                    self._m_schedules.update({'_id': ObjectId(stored['_id'])}, doc)
                    # FIXME: we also need to update the message table
            else:
                doc = {'EncounterID': schedule['Encounterid'],
                       'PatientID': schedule['patientID'],
                       "Location": schedule["Location"],
                       'AppointmentTime': schedule['ApptTime']
                       }
                self._m_schedules.insert_one(doc)
                try:
                    self.create_message_record(schedule)
                except Exception as ex:
                    logger.exception(ex)
                    logger.debug('unable to create message record')
