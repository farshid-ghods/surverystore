#!env/bin/python

from flask import Flask, jsonify, request, g, make_response
from surveystore.logger import Logger
from pymongo import MongoClient
import random
import uuid
from faker import Factory

app = Flask(__name__)
logger = Logger.get_logger(__name__)


@app.route('/')
@app.route('/koodja/')
def index():
    return "Hello, Koodja!"


@app.route('/surveystore/echo', methods=['POST'])
def echo():
    return jsonify({'status': 'success'})

def _fake_date():
    faker = Factory.create()
    year = 2015
    month = "%02d" % 10
    day = "%02d" % 4
    return '%s/%s/%s %s PM' % (month, day, year, faker.time())


@app.route('/surveystore/receive', methods=['POST'])
def receive():
    msg = request.form['Body']
    response = 'Thanks For Your Feedback.'
    mongo = MongoClient('172.30.0.236', 27017)
    survey_db = mongo.surveystore.surveys_2
    try:
        faker = Factory.create()
        score = int(request.form['Body'])
        doc = {
            "PatientID": random.randint(1, 2000),
            "Location": faker.address(),
            "AppointmentTime": _fake_date(),
            "AppointmentDate": faker.date(),
            "EncounterID": random.randint(1000, 4000),
            "Question": "HOW WAS YOUR EXPERIENCE IN THE FOLLOW UP VISITS",
            "Answer": score,
            "Category": "OUTPATIENT",
            "UUID": str(uuid.uuid4())[0:4]
        }
        logger.info(doc)
        survey_db.insert(doc)
    except:
        response = 'please specify number between 1-10'


    return '<?xml version="1.0" encoding="UTF-8"?><Response>' + \
           '<Message>%s</Message>' % response + \
           '</Response>'


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000,
            debug=True)
