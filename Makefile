#MAKEFILE
VIRTBINPATH:=env/bin/
ENVPATH:=env
NODE_MODS:=./node_modules

clean:
	rm -rf $(VIRTBINPATH)
	rm -rf $(ENVPATH)
	rm -rf $(NODE_MODS)

virtualenv:
	test -d $(VIRTBINPATH)/activate || virtualenv env
	chmod +x $(VIRTBINPATH)/activate
	$(VIRTBINPATH)/activate
	$(VIRTBINPATH)/pip install --upgrade pip
	$(VIRTBINPATH)/pip install -Ur requirements.txt --allow-external=TwilioRestClient

init: virtualenv

flake8:
	$(VIRTBINPATH)/flake8 --max-line-length=100 scripts/ functests/ surveystore/

tests: flake8
	$(VIRTBINPATH)/nosetests functests/*

webserver:
	PYTHONPATH=$PYTHONPATH:./ $(VIRTBINPATH)/python surveystore/webserver/websurveystore.py

